﻿# Setup variables
$root = "{C:\Users\ivo\source\repos\SearchInYouTube\SearchInYouTube}"
$FeatureDirectory = "$root\Features"
$OutputDirectory = "{E:\PicklesOutput}"
$DocumentationFormat = "word"

# Import the Pickles-comandlet
Import-Module $root\packages\Pickles.2.20.1\tools\PicklesDoc.Pickles.Powershell.dll

# Call pickles
Pickle-Features -FeatureDirectory $FeatureDirectory `
-11111OutputDirectory $OutputDirectory `
-DocumentationFormat $DocumentationFormat

