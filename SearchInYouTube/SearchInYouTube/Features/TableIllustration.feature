﻿Feature: TableIllustration
	In order to illustrate tables in SpecFlow
	As an application developer
	I want to ensure the functionality of my application


Scenario: Pass data through Specflow tables for StudentInfo object
	Given I have entered following info for Student	
	| FirstName | LastName | Age | YearOfBirth |
	| Koko      | Jumbo    | 20  | 1995        |
	| Rita      | Ore      | 25  | 1990        |
	Then The student should get added to database and entered info should be displayed on the screen
