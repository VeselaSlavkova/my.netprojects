﻿@test
Feature: YouTubeSearchFeature
	In order to test search functionality on you tube
	As a tester
	I want to ensure functionality is working end to end


Background:
	Given I have navigated to YouTube website
Scenario Outline: Youtube should search for the given keyword and should navigate to search results page
	And I have entered <SearchTerm> as search keyword
	When I press the search button
	Then I should be navigated to search results page
	Examples: 
	| SearchTerm |
	| Rebirth    |
	| Inside |
